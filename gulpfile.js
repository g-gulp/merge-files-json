const { src, dest } = require('gulp');
const del = require('del');
var map = require('map-stream');
var merge = require('gulp-merge-json');

function config() {
    var config = {
        output: './src/assets/i18n/merge/',
        files: {
            es: {
                key: 'es',
                input: [{
                    path: './src/assets/i18n/app/es.json',
                    key: 'APP'
                }, {
                    path: './src/assets/i18n/data/es.json',
                    key: 'DATA'
                }]
            },
            en: {
                key: 'en',
                input: [{
                    path: './src/assets/i18n/app/en.json',
                    key: 'APP'
                }, {
                    path: './src/assets/i18n/data/en.json',
                    key: 'DATA'
                }]
            }
        }
    }   
    return config;
}

function concatKey(output, key) {
    src(`${output}**/${key}.json`)
        .pipe(merge({
            fileName: `${key}.json`
        }))
        .pipe(dest(output));
}

function changeText(inputPath, outputPath, prefix, suffix) {
    let fileContents = '';
    src(inputPath)
        .pipe(map(function(file, cb) {
            fileContents = file.contents.toString();
            fileContents = prefix + fileContents + suffix;
            file.contents = new Buffer(fileContents);
            cb(null, file);
        }))
        .pipe(dest(outputPath));
};

function build(cb) {
    (async () => {
        const deletedPaths = await del([config().output]);     
        console.log('Deleted files and directories:\n', deletedPaths.join('\n'));
        for (let language in config().files) {            
            language = config().files[language];
            inputs = language.input;
            inputs.forEach(input => {
                const prefix = '{"' + input.key + '":';
                const suffix = '}';
                const outputPath = `${config().output}${input.key}`;
                changeText(input.path, outputPath, prefix, suffix);
            });
            setTimeout(() => {
                concatKey(config().output, language.key);
            }, 10000);
        }
        cb();
    })();
}

exports.default = build;
