# merge-files-json

Crea objetos JSON a partir de la unión de objetos JSON declarados en distintos archivos.

## Conceptos a desarrollar

Conceptos: Gulp, JSON

## Tecnologias

Lenguaje: JavaScript

Task Runner: Gulp

## Uso

Para su uso ejecutar los siguientes comandos:

```bash
1) npm install
2) gulp
```


